CC=gcc
CFLAGS=-g -Wall -pedantic

SRC=src
SRCS=$(wildcard $(SRC)/*.c)
HDR=hdrs

OBJ=obj
OBJS=$(patsubst $(SRC)/%.c, $(OBJ)/%.o, $(SRCS))

LIBDIR=lib
LIB=$(LIBDIR)/libqu.a

TEST=tests
TESTS=$(wildcard $(TEST)/*.c)
TESTBINS=$(patsubst $(TEST)/%.c, $(TEST)/bin/%, $(TESTS))


###
all:$(LIB)

release: CFLAGS=-Wall -Werror -O2 -DNDEBUG -pedantic
release: clean
release: $(LIB)

tests: $(LIB) $(TEST)/bin $(TESTBINS)
	for test in $(TESTBINS) ; do ./$$test --verbose ; done


###
$(LIB): $(LIBDIR) $(OBJ) $(OBJS)
	$(RM) $(LIB)
	ar -cvrs $(LIB) $(OBJS)

$(OBJ)/%.o: $(SRC)/%.o $(HDR)/%.h
	$(CC) $(CFLAGS) -std=c90 -c $< -o $@

$(OBJ)/%.o: $(SRC)/%.c
	$(CC) $(CFLAGS) -std=c90 -c $< -o $@

$(TEST)/bin/%: $(TEST)/%.c
	$(CC) $(CFLAGS) $< $(OBJS) -o $@ -lcriterion


###
$(OBJ):
	mkdir $@

$(LIBDIR):
	mkdir $@

$(TEST)/bin:
	mkdir $@


###
clean:
	$(RM) -r $(LIBDIR) $(OBJ) $(TEST)/bin


###
.PHONY: all release tests clean
