#include "../hdrs/qu.h"


/*
 * Creates a queue.
 * 
 * DESCRIPTION:
 * Dynamically allocates memory,
 * sets inital values and 
 * returns a pointer.
 * 
 * ARGUMENTS:
 * struct queue *q_orig - source
 * struct queue *q_copy - destination
 *
 * RETURN:
 * q - pointer to struct
 */
struct queue *
qu_create(size_t max_size) {
     struct queue *q = malloc(sizeof(struct queue));
     q->size = max_size;
     q->values = malloc(sizeof(int) * q->size);
     q->n_elem = 0;
     q->head = 0;
     q->tail = 0;

     return q;
}


/*
 * Copies the contents of one queue to
 * a queue of the same size.
 * 
 * DESCRIPTION:
 * Does a one to one assignment of
 * all attribues.
 * The sizes of both queues must be identical
 * 
 * ARGUMENTS:
 * struct queue *q_orig - source
 * struct queue *q_copy - destination
 *
 * RETURN:
 * 1 if mismatch of size, otherwise 0.
 */
int
qu_copy(struct queue *q_orig, struct queue *q_copy) {
     int i;

     if(q_orig->size != q_copy->size) {
	  return 1;
     }

     /* copy array or values from q_orig to q_copy */
     for(i = 0; i < q_copy->size; i++) {
	  q_copy->values[i] = q_orig->values[i];
     }
     q_copy->n_elem = q_orig->n_elem;
     q_copy->head = q_orig->head;
     q_copy->tail = q_orig->tail;

     return 0;
}


/*
 * Free the memory of a queue.
 * 
 * ARGUMENTS:
 * struct queue *q
 */
void
qu_destroy(struct queue *q) {
     free(q->values);
     free(q);
}

/*
 * Checks if a queue has any
 * elements pushed to values array.
 *
 * ARGUMENTS:
 * struct queue *q
 *
 * RETURN:
 * int - 1 if empty, otherwise 0.
 */
int
qu_empty(const struct queue *q) {
     return q->n_elem == 0;
}

/*
 * Checks if a queue is full.
 *
 * ARGUMENTS:
 * struct queue *q
 *
 * RETURN:
 * int - 1 if full, otherwise 0.
 */
int
qu_full(const struct queue *q) {
     return q->n_elem == q->size;
}

/*
 * Enqueue
 * 
 * ARGUMENTS:
 * struct queue *q
 * int value - Integer to append to queue.
 * 
 * RETURN:
 * If empty QUEUE_EMPTY (INT_MIN),
 * otherwise return the front of the queue.
 */
int
qu_enqueue(struct queue *q, int value){
     if(qu_full(q)) return 0;

     q->values[q->tail] = value;
     q->n_elem++;
     q->tail = (q->tail + 1) % q->size; /* wrap tail round to zero */
     
     return 1;
}

/*
 * Dequeue
 * 
 * ARGUMENTS:
 * struct queue *q
 * 
 * RETURN:
 * If empty QUEUE_EMPTY (INT_MIN),
 * otherwise return the front of the queue.
 */
int
qu_dequeue(struct queue *q) {
     int result;

     if(qu_empty(q)) {
	  return QUEUE_EMPTY;
     }

     result = q->values[q->head];
     q->head = (q->head + 1) % q->size;
     q->n_elem--;
     
     return result;
}

/*
 * Rotates a queue.
 * 
 * TODO:
 * -- Consider head and tail position
 * -- Enact the rotation n times
 * 
 * DESCRIPTION:
 * Rotates queue clockwise by swapping 
 * consecutive values in the values array 
 * and wrapping last element.
 * 
 * ARGUMENTS:
 * struct queue *q
 * int n - Rotations to the left.
 * 
 * RETURN:
 * 0 if empty, otherwise 1.
 */
int
qu_rotate(struct queue *q, int n) {
     int i;
     int pos;
     int tmp;

     if(qu_empty(q)) return 0;
     
     tmp = q->values[0];
     for(i = 0; i < q->n_elem; i++) {
	  pos = (i+1) % (q->n_elem); /* % q->n_elem - 1 ? */
	  q->values[i] = q->values[pos];
     }
     q->values[q->n_elem-1] = tmp;

     return 1;
}

/*
 * Reverse a queue.
 * 
 * TODO:
 * -- Consider head and tail position
 * 
 * DESCRIPTION:
 * Rotates queue clockwise by swapping 
 * consecutive values in the values array 
 * and wrapping last element.
 * 
 * ARGUMENTS:
 * struct queue *q
 * 
 * RETURN:
 * 0 if empty, otherwise 1.
 */
