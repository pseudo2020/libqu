#include <stdio.h>
#include <criterion/criterion.h>

#include "../hdrs/qu.h"


/* 'qu_tests' Suite setup */
const size_t QUSIZE = 6;
struct queue *q = NULL;

void suite_setup(void) {
     int i;
     q = qu_create(QUSIZE);

     for(i = 1; i <= QUSIZE; i++) {
	  qu_enqueue(q, i);
     }
}

void suite_teardown(void) {
     qu_destroy(q);
}

int add_items(int random) {
     /* Fills the queue with a QUSIZE amount of data */
     /* set random to 1 for random (0-255) numbers, else in order */
     
}

TestSuite(qu_tests, .init=suite_setup, .fini=suite_teardown);


/* 'qu_tests' Suite unit tests */
Test(qu_tests, creation) {
     cr_expect(q != NULL,
	       "\'qu_create\' should return a non-NULL value.");
}

Test(qu_tests, append_too_many_elements) {
     cr_expect(qu_enqueue(q, QUSIZE+1) == 0,
	       "Expected enqueue to fail.");
}

Test(qu_tests, remove_elements) {
     int i;

     for(i = 0; i < QUSIZE; i++) {
	  cr_expect(qu_dequeue(q) != QUEUE_EMPTY,
		    "Expected dequeue to succeed.");
     }

     cr_expect(qu_dequeue(q) == QUEUE_EMPTY,
		    "Expected dequeue to succeed.");
}


Test(qu_tests, copy_queue) {
     /* struct queue *copy; */
     
     /* copy = qu_copy(q); */
}
 
Test(qu_tests, rotate_queue) {
     /* int i; */
     
     /* qu_rotate(q, 1); */

     /* for(i = 0; i < QUSIZE; i++) { */
	  /* item = q->values[j]; */
	  /* printf("j: %d\titem: %d\n", j, item); */
	  /* cr_expect(item,  */
          /* "Expected to have rotated 1 to the left."); */
     /* } */
}

