#ifndef _LIBQU_H_
#define _LIBQU_H_


#include <stdlib.h>
#include <limits.h>


#define QUEUE_EMPTY INT_MIN

struct queue {
     int *values;
     int head, tail;
     size_t n_elem;
     size_t size;
};

struct queue * qu_create(size_t max_size);
int qu_copy(struct queue *q_orig, struct queue *q_copy);
void qu_destroy(struct queue *q);

int qu_empty(const struct queue *q);
int qu_full(const struct queue *q);
int qu_enqueue(struct queue *q, int value);
int qu_dequeue(struct queue *q);
void rotate(struct queue *q, int n);

#endif /* _LIBQU_H_ */
